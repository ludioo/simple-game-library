#pragma once
#include "Position.h"

enum Speed { LOW = 1, MEDIUM = 2, HIGH = 4 };

class Player {
    private:
        Position position;
        char skin[4][12] = {
            { ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ' },
            { ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ' },
            { ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ' },
            { ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ' },
        };
        int side = 1;
        int speed = Speed::LOW;
        int score = 0;
        
    public:
        Player();
        Player(char[4][12], int = 1, int = Speed::LOW);
        void setSkin(char[4][12]);
        void setSide(int);
        Position getPosition();
        void setPosition(int, int);
        int getSpeed();
        void setSpeed(int);
        int getScore();
        void addScore(int);

        void draw(bool = true);
        void clear();
        void moveLeft();
        void moveRight();
};