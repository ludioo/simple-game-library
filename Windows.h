#ifndef MY_HEADER
#define MY_HEADER 1

#include <windows.h>
#include <conio.h>

inline void gotoxy(int x, int y) {
  COORD coord;
  coord.X = x;
  coord.Y = y;
  SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

inline void clear() {
  system("cls");
}

#endif