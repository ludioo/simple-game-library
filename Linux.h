#ifndef LINUX_HEADER
#define LINUX_HEADER 1

/*
AUTHOR: zobayer
INSTRUCTION:
just make this file a header like "conio.h"
and use the getch() and getche() functions.
*/

#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/select.h>
#include <sys/ioctl.h>
#include <stropts.h>

/* reads from keypress, doesn't echo */
inline int getch(void)
{
    struct termios oldattr, newattr;
    int ch;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
    return ch;
}

/* reads from keypress, and echoes */
inline int getche(void)
{
    struct termios oldattr, newattr;
    int ch;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~(ICANON);
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
    return ch;
}

inline int kbhit()
{
    static const int STDIN = 0;
    static bool initialized = false;

    if (!initialized)
    {
        // Use termios to turn off line buffering
        termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}

inline void delay(int num)
{
    fflush(stdout);
    usleep(num * 1000);
}

inline void gotoxy(int x, int y)
{
    printf("%c[%d;%df", 0x1B, y, x);
}

// Clear screen in multi platform
inline void clear()
{
    #ifdef __unix__
        system("clear");
    #else
        system("cls");
    #endif
}

#endif