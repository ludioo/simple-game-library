#include "Obstacle.h"
#include <vector>

struct Stack {
    Obstacle *obstacle;
};

class ObstacleFactory {
    private:
        std::vector<Stack> stacks;
    public:
        Obstacle *getObstacle(Obstacle *);
};