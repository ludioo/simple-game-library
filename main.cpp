#include "Manager.h"

int main(int argc, char const *argv[])
{
    char arenaSkin = '*';
    char obstacleSkin[4][4] = {
        { ' ', '#', '#', ' ' },
        { ' ', '#', '#', ' ' },
        { ' ', '#', '#', ' ' },
        { ' ', '#', '#', ' ' }
    };
    char playerSkin[4][12] = {
        { ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ' },
    };

    Arena arena(arenaSkin);
    Obstacle obstacle(obstacleSkin);
    Player player(playerSkin);

    Manager manager(&arena, &obstacle, &player);
    manager.play();
}