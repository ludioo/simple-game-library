# Dokumentasi

Beberapa Class yang ada dalam library :
* Class Player ( Digunakan untuk mengatur behavior pada Objek Pemain )
* Class Arena ( Digunakan untuk membuat Map pada Game )
* Class Obstacle ( Digunakan untuk mengatur behavior pada Objek Rintangan / Obstacle )
* Class Manager ( Sebagai Game Controller / mengatur jalannya Permainan )

# Class Player

Beberapa atribut yang dimiliki oleh class Player :
* Char skin[4][12] : Sebagai bentuk karakter dari objek player ( default value = array of char 2 Dimensi 12x4 )
* Int side : Untuk meletakkan player pada jalur satu, dua, atau tiga ( default value = 1 )
* Int speed : Sebagai tingkat kesulitan dari game ( default value = 50 )

Class Player memiliki beberapa cara penggunaan, antara lain adalah :
* Penggunaan melalui Constructor
	* Anda dapat secara langsung melakukan custom nilai dalam game sesuai keinginan melalui Constructor class. Beberapa parameter yang harus diperhatikan antara lain yaitu skin ( char[4][12] ), side ( int ), dan speed ( int ). Contoh penggunaan :
	<img src="image/carbon.png" width="750">
* Penggunaan melalui Getter and Setter
	* Anda juga dapat melakukan custom nilai melalui Getter and Setter yang telah disediakan, antara lain setSkin(char skin[4][12]), setSide(int side), dan setSpeed(int speed). Contoh penggunaan : 
	<img src="image/carbon1.png" width="750">

# Class Arena

Beberapa atribut yang dimiliki oleh class Player :
* Char skin : Sebagai bentuk karakter dari objek arena ( default value = ‘*’ )

Class Arena memiliki beberapa cara penggunaan, antara lain adalah :
* Penggunaan melalui Constructor
	* Beberapa parameter yang harus diperhatikan yaitu skin ( char ). Contoh penggunaan :
	<img src="image/carbon2.png" width="400">
* Penggunaan melalui Getter and Setter
	* Anda juga dapat melakukan custom nilai melalui Getter and Setter yang telah disediakan yaitu setSkin(char skin). Contoh penggunaan : 
	<img src="image/carbon3.png" width="400">

# Class Obstacle

Beberapa atribut yang dimiliki oleh class Obstacle :
* Char skin[4][4] : Sebagai bentuk karakter dari objek obstacle ( default value = array of char 2 Dimensi 4x4 )

Class Obstacle memiliki beberapa cara penggunaan, antara lain adalah :
* Penggunaan melalui Constructor
	* Anda dapat secara langsung melakukan custom skin obstacle sesuai keinginan melalui Constructor class. Contoh penggunaan :
	<img src="image/carbon4.png" width="500">
* Penggunaan melalui Getter and Setter
	* Anda juga dapat melakukan custom nilai skin melalui Getter and Setter yang telah disediakan yaitu setSkin(char skin[4][4]). Contoh penggunaan : 
	<img src="image/carbon5.png" width="500">

# Class Manager

Beberapa atribut yang dimiliki oleh class Manager :
* Arena *arena : Sebagai pointer dari objek arena ( yang akan dicustom oleh user pada file utama )
* Obstacle *obstacle : Sebagai pointer dari objek arena ( yang akan dicustom oleh user pada file utama )
* Player *player : Sebagai pointer dari objek player ( yang akan dicustom oleh user pada file utama )

Beberapa behavior yang dimiliki oleh class Manager :
* Void play() : Digunakan untuk menjalankan game

Class Player hanya memiliki satu cara penggunaan, yaitu melalui Constructor. Setiap parameter pada Constructor wajib diisi dengan pointer dari objek yang telah dibuat oleh user. Contoh penggunaan :
<br><br><img src="image/carbon6.png" width="500">


