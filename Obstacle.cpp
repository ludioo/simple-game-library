#include <iostream>
#include "Obstacle.h"

#ifdef __unix__
    #include "Linux.h"
#else
    #include "Windows.h"
#endif

using namespace std;

Obstacle::Obstacle() {}

Obstacle::Obstacle(char skin[4][4], int x, int y) {
    setSkin(skin);
    setPosition(x, y);
}

void Obstacle::setSkin(char skin[4][4]) {
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            this->skin[i][j] = skin[i][j];
        }
    }
}

void Obstacle::setPosition(int x, int y) {
    this->position.x = x;
    this->position.y = y;
}

void Obstacle::draw(bool status) {
    int y = position.y;
    
    // Maximum y position for Arena
    int maxY = 32;
    // Maximum Obstacle height printed
    int iMax = 4;

    if (y >= maxY - 2) {
        iMax = (maxY - y + 1);
    }

    for (int i = 0; i < iMax; i++)
    {
        int x = position.x;

        for (int j = 0; j < 4; j++)
        {
            gotoxy(x, y);
            
            if (status) {
                cout << skin[i][j];
            }
            else {
                cout << " ";
            }   
            
            x += 1;
        }

        y += 1;

        cout << endl;
    }

    if(position.y > maxY) {
        isActive = false;
        //7, 21, 36
    }
}

Position Obstacle::getPosition() {
    return this->position;
}

bool Obstacle::getActive() {
    return this->isActive;
}

void Obstacle::clear() {
    draw(false);
}

void Obstacle::moveDown() {
    position.y++;
}

void Obstacle::update() {
    this->clear();
    this->moveDown();
    this->draw();
}

void Obstacle::resetPosition(int random) {
    isActive = true;
    this->setPosition(random % 3 == 1 ? 7 : random % 3 == 2 ? 21 : 36, 3);
}