#pragma once
#include "Arena.h"
#include "Obstacle.h"
#include "Player.h"

class Manager {
    private:
        Arena *arena;
        Obstacle *obstacle;
        Obstacle obstacles[2];
        Player *player;
    public:
        Manager();
        Manager(Arena *, Obstacle *, Player *);

        void play();
        void listen();
        void updateScore();
        void obstaclesHandle(float *);
        bool checkCollision();
        bool gameOver();
        void reset();
};