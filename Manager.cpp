#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "ObstacleFactory.h"
#include "Manager.h"

#ifdef __unix__
    #include "Linux.h"
#else
    #include "Windows.h"
#endif

using namespace std;

Manager::Manager() {}

Manager::Manager(Arena *arena, Obstacle *obstacle, Player *player) {
    this->arena = arena;
    this->obstacle = obstacle;
    this->player = player;

    srand(time(NULL));
}

void Manager::play() {
    clear();

    if (!this->arena && !this->obstacle && !this->player) {
        cout << " =============================================================" << endl;
        cout << " =                       Library Error                       =" << endl;
        cout << " =                                                           =" << endl;
        cout << " =    You Must Initialize Manager with Pointer of Arena,     =" << endl;
        cout << " =                    Obstacle, and Player                   =" << endl;
        cout << " =============================================================" << endl;
        exit(0);
    }

    cout << " =============================================================" << endl;
    cout << " =                   Welcome to the Game                     =" << endl;
    cout << " =                                                           =" << endl;
    cout << " =             Use a or d key for Move Character             =" << endl;
    cout << " =                                                           =" << endl;
    cout << " =                  Press any key to Start                   =" << endl;
    cout << " =============================================================" << endl;

    bool playAgain = false;

    do {
        getch();
        clear();
        
        arena->draw();
        player->draw();
        
        float counter = 0;
        float counterObstacle = 4000000;

        while (true) {
            listen();
            usleep(100000 / player->getSpeed());

            this->obstaclesHandle(&counterObstacle);
            this->updateScore();

            counter++;
            counterObstacle++;
            gotoxy(0, 33);

            if (checkCollision())
                break;
        }

        // print UI and ask play again
        playAgain = gameOver();
    }
    while(playAgain);
}

bool Manager::checkCollision() {
    for (int i = 0; i < 2; i++) {
        if (!obstacles[i].getActive())
            continue;

        int obsX = obstacles[i].getPosition().x;
        int plyX = player->getPosition().x;
        bool xStatus = obsX == 7 && plyX == 3 || obsX == 21 && plyX == 18 || obsX == 36 && plyX == 33;
        bool yStatus = obstacles[i].getPosition().y + 3 >= player->getPosition().y;

        if (xStatus && yStatus)
            return true;
    }

    return false;
}

bool Manager::gameOver() {
    clear();
    gotoxy(0, 0);
    cout << " =============================================================" << endl;
    cout << " =                        Game Over                          =" << endl;
    cout << " =                                                           =" << endl;
    cout << "                          Score: " << player->getScore() << "                         " << endl;
    cout << " =                                                           =" << endl;
    cout << " =                 Press Space to Play Again                 =" << endl;
    cout << " =============================================================" << endl;

    while(!(!(!(false))) == !false || 1) { // (:
        if (kbhit()) {
            switch (getch()) {
                case 32: 
                    reset();
                    return true;
                    break;

                default: 
                    return false;
                    break;
            }
        }
    }
}

void Manager::reset() {
    player->addScore(-player->getScore());
    for (int i = 0; i < 2; i++) {
        obstacles[i].setPosition(0, 0);
    }
}

void Manager::obstaclesHandle(float *counterObstacle) {
    ObstacleFactory factory;
    for (int i = 0; i < 2; i++) {
        if (obstacles[i].getPosition().x == 0 && *counterObstacle >= 15) {
            obstacles[i] = *factory.getObstacle(obstacle);
            obstacles[i].resetPosition(rand() % 12 + 1);

            *counterObstacle = 0;
        }
        else if (obstacles[i].getPosition().x != 0 && !obstacles[i].getActive())
            obstacles[i].resetPosition(rand() % 12 + 1);
        else if (obstacles[i].getPosition().x == 0 && *counterObstacle < 15)
            continue;

        obstacles[i].update();
    }
}

void Manager::listen() {
    if (kbhit()) {
        gotoxy(0, 33);
        cout << "                                     ";

        switch (getch())
        {
            case 'a': 
                player->clear(); 
                player->moveLeft(); 
                player->draw();
                break;

            case 'd': 
                player->clear(); 
                player->moveRight(); 
                player->draw();
                break;
        }
    }
}

void Manager::updateScore() {
    gotoxy(0, 0);
    cout << "                                 ";
    gotoxy(0, 0);
    player->addScore(10);
    cout << "Score : " << player->getScore();
}

int random(int min, int max) 
{
    return rand() % max + min ;
}