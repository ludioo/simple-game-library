#include <iostream>
#include "Player.h"

#ifdef __unix__
    #include "Linux.h"
#else
    #include "Windows.h"
#endif

using namespace std;

Player::Player() {
    setPosition(3 + (15 * (this->side - 1)), 29);
}

Player::Player(char skin[4][12], int side, int speed) {
    setSkin(skin);
    setSide(side);
    setSpeed(speed);
}

void Player::setSkin(char skin[4][12]) {
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 12; j++)
        {
            this->skin[i][j] = skin[i][j];
        }
    }
}

void Player::setSide(int side) {
    this->side = (side < 1 || side > 3) ? 1 : side;
    setPosition(3 + (15 * (this->side - 1)), 29);
}

Position Player::getPosition() {
    return position;
}

void Player::setPosition(int x, int y) {
    this->position.x = x;
    this->position.y = y;
}

int Player::getSpeed() {
    return speed;
}

void Player::setSpeed(int speed) {
    this->speed = speed;
}

int Player::getScore() {
    return this->score;
}

void Player::addScore(int score) {
    this->score += score;
}

void Player::draw(bool status) {
    int y = position.y;

    for (int i = 0; i < 4; i++)
    {
        int x = position.x;

        for (int j = 0; j < 12; j++)
        {
            gotoxy(x, y);
            
            if (status) {
                cout << skin[i][j];
            }
            else {
                cout << " ";
            }   
            
            x += 1;
        }

        y += 1;

        cout << endl;
    }
}

void Player::clear() {
    draw(false);
}

void Player::moveLeft() {
    if (position.x > 3) {
        position.x -= 15;
    }
}

void Player::moveRight() {
    if (position.x < 33) {
        position.x += 15;
    }
}