#include <iostream>
#include "Arena.h"

#ifdef __unix__
    #include "Linux.h"
#else
    #include "Windows.h"
#endif

using namespace std;

enum { SPACE = 0, WALL = 1, PLAYER = 2 };

Arena::Arena(char skinChar) {
    setSkin(skinChar);
}

void Arena::setSkin(char skinChar) {
    this->skinChar = skinChar;
}

void Arena::draw() {
    gotoxy(0, 3);

    for (int i = 0; i < 30; i++)
    {
        for (int j = 0; j < 46; j++)
        {
            switch (skin[i][j])
            {
                case SPACE: cout << " "; break;
                case WALL: cout << skinChar; break;
                default: break;
            }
        }

        cout << endl;
    }
}

bool Arena::isWall(Player *player, bool isLeft) {
    Position pos = player->getPosition();

    if (isLeft) {
        return skin[pos.y][pos.x--] == WALL;
    }
    
    return skin[pos.y][pos.x++] == WALL;
}