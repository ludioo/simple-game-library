#include "ObstacleFactory.h"

Obstacle *ObstacleFactory::getObstacle(Obstacle *obs) {
    if (!stacks.empty()) {
        return stacks.back().obstacle;
    }
    else {
        Stack stack;
        stack.obstacle = obs;
        stacks.push_back(stack);
        return stack.obstacle;
    }
}