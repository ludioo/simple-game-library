#pragma once
#include "Position.h"

class Obstacle {
    private:
        bool isActive;
        Position position;
        char skin[4][4] = {
            { ' ', '#', '#', ' ' },
            { ' ', '#', '#', ' ' },
            { ' ', '#', '#', ' ' },
            { ' ', '#', '#', ' ' }
        };
    public:
        Obstacle();
        Obstacle(char[4][4], int = 7, int = 3);

        bool getActive();
        Position getPosition();
        void setSkin(char[4][4]);
        void setPosition(int, int);
        void draw(bool = true);
        void clear();
        void moveDown();
        void update();
        void resetPosition(int);
};